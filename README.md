## Gradle Docker container

## Base Docker image
* [java](https://hub.docker.com/_/java/)

# Docker tags
`deltatrees/gradle` provides multiple tagged images:

* `latest` (default) alias to `2.8`
* `2.8` Gradle version 2.8
* `2.6` Gradle version 2.6

## Installation

1. Install [Docker](https://www.docker.com/).
2. Download [automated build](https://hub.docker.com/r/deltatrees/gradle/) from public [Docker Hub](https://hub.docker.com/explore/): `docker pull deltatrees/gradle`

## Usage
```
docker run deltatrees/gradle gradle command
```
